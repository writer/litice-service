package com.litice.common.server.bean;

import io.swagger.annotations.ApiModelProperty;

public class TestBean {
    @ApiModelProperty("姓名")
    public String name;
    @ApiModelProperty("年龄")
    public Integer age;
    @ApiModelProperty("性别")
    public String sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
