package com.litice.common.server.controller;

/**
 * @program: litice-service
 * @description: redis连接测试
 * @author: powerWriter
 * @create: 2019-06-07 12:32
 **/

import com.litice.common.server.utils.RedisUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/redisDemo")
@Api(value = "redisDemo", description = "redisDemo")
public class RedisController {

    @Autowired
    public RedisUtil redisUtil;

    @ApiOperation(value = "登录接口-多值传输方式", notes = "输入用户名和密码登录")
    @RequestMapping(value = "add", method = RequestMethod.POST, produces= MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> loginForMap(@RequestParam String key, @RequestParam String value) {
        redisUtil.set(key, value);
        return ResponseEntity.ok("添加成功");
    }
}
