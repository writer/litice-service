package com.litice.common.server.dao;

import com.litice.common.server.bean.UserBean;

import java.util.List;

public interface MongoDAO {

    List<UserBean> getAllUsers();

    void updateUser();

    void deleteUser();

    void addUsers();

}
