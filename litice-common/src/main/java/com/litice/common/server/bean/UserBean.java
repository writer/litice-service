package com.litice.common.server.bean;

/**
 * @program: litice-service
 * @description: 用户实体类
 * @author: powerWriter
 * @create: 2019-06-07 12:37
 **/
public class UserBean
{
    public String id;

    public String username;

    public String realname;

    public String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
