package com.litice.common.server.dao.Impl;

import com.litice.common.server.bean.UserBean;
import com.litice.common.server.dao.MongoDAO;

import java.util.List;

/**
 * @program: litice-service
 * @description: mongoDao实现类
 * @author: powerWriter
 * @create: 2019-06-07 12:34
 **/
public class MongoDaoImpl implements MongoDAO {
    @Override
    public List<UserBean> getAllUsers() {
        return null;
    }

    @Override
    public void updateUser() {

    }

    @Override
    public void deleteUser() {

    }

    @Override
    public void addUsers() {

    }
}
