package com.litice.web;

import com.litice.web.swaggerConfig.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2Doc
@SpringBootApplication(scanBasePackages={"com.*"})
@EnableSwagger2
//@ImportResource(locations={"classpath:spring-minidao.xml"})
//@MapperScan({"com.*"})
public class RunApplication extends SpringBootServletInitializer {

	/**
	 * 启动入口
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		SpringApplication.run(RunApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RunApplication.class);
	}

}
