package com.litice.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author powerwriter
 * Create Date： 2019/01/19.
 *
 * 重定向swagger api的主页，访问地址：http://127.0.0.1:8080/api
 */
@Api(value = "Swagger主页", description = "Swagger主页")
@RequestMapping("/")
@RestController
public class IndexController {

/*    @ApiOperation(value = "Hello Spring Boot", notes = "Hello Spring Boot")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public void index(HttpServletResponse response) {
        response.sendRedirect("swagger-ui.html");
    }*/

    @ApiOperation(value = "API 页面", notes = "接口页面")
    @RequestMapping(value = "/api", method = RequestMethod.GET)
    public void api(HttpServletResponse response) throws IOException {
        response.sendRedirect("swagger-ui.html");
    }
}
