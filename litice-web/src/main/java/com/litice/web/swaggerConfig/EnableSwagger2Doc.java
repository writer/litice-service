package com.litice.web.swaggerConfig;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author powerwriter
 * Create Date： 2019/01/19.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({SwaggerAutoConfiguration.class})
public @interface EnableSwagger2Doc {


}
