package com.litice.web.swaggerConfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.resource.PathResourceResolver;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;
import org.springframework.web.util.UrlPathHelper;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.ServletContext;
import java.security.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author powerwriter
 * Create Date： 2019/01/19.
 */

@Configuration
@EnableSwagger2
public class Swagger2Config {
    public static final String BASE_PACKAGE = "com.litice.*";
    private static final String DEFAULT_PATH = "/api";

    @Value("${swagger.enabled}")
    private boolean enableSwagger = false;
    /*@Bean
    public Docket CreateRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(enableSwagger)      // 生产环境的时候关闭 swagger 比较安全
                .directModelSubstitute(Timestamp.class, Long.class)                //将Timestamp类型全部转为Long类型
                .directModelSubstitute(Date.class, Long.class) //将Date类型全部转为Long类型
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))// 扫描接口的包路径，不要忘记改成自己的
                .paths(PathSelectors.any())
                .build();
    }*/
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("Swagger RESTful APIs")
            .description("Swagger API 服务")
            .termsOfServiceUrl("http://swagger.io/")
            .contact(new Contact("powerWriter", "https://gitee.com/writer/events", "mhc2018@qq.com"))
            .version("1.0")
            .build();
    }

}
